package com.example.eataja;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

public class HomeFragment extends Fragment {
    DatabaseReference reference;
    String USERNAME_KEY = "usernamekey";
    String username_key = "";
    String username_key_new = "";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        LinearLayout ayamboss, lawlessburger, thaitea, nasiuduk;
        ayamboss = view.findViewById(R.id.ayamboss);
        lawlessburger = view.findViewById(R.id.lawlessburger);
        thaitea = view.findViewById(R.id.thaitea);
        nasiuduk = view.findViewById(R.id.nasiuduk);

        ayamboss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoayamboss = new Intent(v.getContext(), PickMenuAct.class);
                gotoayamboss.putExtra("toko", "ayamboss");
                startActivity(gotoayamboss);
            }
        });

        lawlessburger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotolawless = new Intent(v.getContext(), PickMenuAct.class);
                gotolawless.putExtra("toko", "lawlessburger");
                startActivity(gotolawless);
            }
        });

        thaitea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotothai = new Intent(v.getContext(), PickMenuAct.class);
                gotothai.putExtra("toko", "thaitea");
                startActivity(gotothai);
            }
        });

        nasiuduk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotonasi = new Intent(v.getContext(), PickMenuAct.class);
                gotonasi.putExtra("toko", "nasiuduk");
                startActivity(gotonasi);
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getUsernameLocal();


        TextView points = getView().findViewById(R.id.points);


        reference = FirebaseDatabase.getInstance().getReference().child("Users").child("customer").child(username_key_new);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final TextView points = getView().findViewById(R.id.points);
                points.setText(dataSnapshot.child("user_points").getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




    }

    public void getUsernameLocal(){
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(USERNAME_KEY, Context.MODE_PRIVATE);
        username_key_new = sharedPreferences.getString(username_key, "");
    }

}
