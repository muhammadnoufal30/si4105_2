package com.example.eataja;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class CartAct extends AppCompatActivity {
    DatabaseReference reference;
    ImageView image_cart;
    TextView title_cart, subtitle_cart, cart_harga, tv_jumlahorder;
    Button btnmines, btnplus, btn_add_cart;
    Integer jumlahorder = 0;

    String USERNAME_KEY = "usernamekey";
    String username_key = "";
    String username_key_new = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        getUsernameLocal();

        subtitle_cart = findViewById(R.id.subtitle_cart);
        image_cart = findViewById(R.id.image_cart);
        cart_harga = findViewById(R.id.cart_harga);
        title_cart = findViewById(R.id.title_cart);
        btnmines = findViewById(R.id.btnmines);
        btnplus = findViewById(R.id.btnplus);
        btn_add_cart = findViewById(R.id.btn_add_cart);
        tv_jumlahorder = findViewById(R.id.tv_jumlahorder);

        Bundle bundle = getIntent().getExtras();
        final String cart = bundle.getString("cart");

        btnmines.animate().alpha(0).setDuration(300).start();
        btnmines.setEnabled(false);
        btnmines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumlahorder -=1;
                Toast.makeText(CartAct.this, "Ini " + cart, Toast.LENGTH_SHORT).show();
                tv_jumlahorder.setText(jumlahorder.toString());

                if(jumlahorder == 0){
                    btn_add_cart.setVisibility(View.INVISIBLE);
                }else{
                    btn_add_cart.setVisibility(View.VISIBLE);
                    btn_add_cart.setText("Tambahkan ke keranjang (" + jumlahorder.toString() +")");
                }

                if(jumlahorder < 1){
                    btnmines.animate().alpha(0).setDuration(300).start();
                    btnmines.setEnabled(false);
                }
            }
        });

        btnplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumlahorder += 1;
                tv_jumlahorder.setText(jumlahorder.toString());
                if(jumlahorder > 0){
                    btnmines.animate().alpha(1).setDuration(300).start();
                    btnmines.setEnabled(true);
                    btn_add_cart.setVisibility(View.VISIBLE);
                    btn_add_cart.setText("Tambahkan ke keranjang (" + jumlahorder.toString() +")");
                }
            }
        });

        reference = FirebaseDatabase.getInstance().getReference().child("menu").child(cart);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                title_cart.setText(dataSnapshot.child("nama").getValue().toString());
                subtitle_cart.setText(dataSnapshot.child("deskripsi").getValue().toString());
                cart_harga.setText(dataSnapshot.child("harga").getValue().toString());
                Picasso.get().load(dataSnapshot.child("foto").getValue().toString())
                        .resize(420, 230).centerCrop().into(image_cart);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        btn_add_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CartAct.this, OrderDetailAct.class);
                intent.putExtra("nama_pemesan", username_key_new);
                intent.putExtra("jumlah_order", tv_jumlahorder.getText().toString());
                startActivity(intent);
            }
        });
    }

    public void getUsernameLocal(){
        SharedPreferences sharedPreferences = getSharedPreferences(USERNAME_KEY, Context.MODE_PRIVATE);
        username_key_new = sharedPreferences.getString(username_key, "");
    }
}
