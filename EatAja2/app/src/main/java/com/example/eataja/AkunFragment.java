package com.example.eataja;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AkunFragment extends Fragment {
    DatabaseReference reference;
    String USERNAME_KEY = "usernamekey";
    String username_key = "";
    String username_key_new = "";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_akun, container, false);

        TextView tv_editprofil = view.findViewById(R.id.tv_editprofil);

        tv_editprofil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ProfileAct.class);
                startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getUsernameLocal();

        reference =  FirebaseDatabase.getInstance().getReference().child("Users").child("customer").child(username_key_new);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final TextView tv_name = getView().findViewById(R.id.tv_name);
                final TextView tv_points = getView().findViewById(R.id.tv_points);

                tv_name.setText(dataSnapshot.child("nama_lengkap").getValue().toString());
                tv_points.setText(dataSnapshot.child("user_points").getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void getUsernameLocal(){
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(USERNAME_KEY, Context.MODE_PRIVATE);
        username_key_new = sharedPreferences.getString(username_key, "");
    }
}
