package com.example.eataja;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ProfileAct extends AppCompatActivity {
    DatabaseReference reference;
    String USERNAME_KEY = "usernamekey";
    String username_key = "";
    String username_key_new = "";
    EditText profile_email, profile_name, profile_password;
    Button simpan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getUsernameLocal();

        simpan = findViewById(R.id.simpan);
        profile_email = findViewById(R.id.profile_email);
        profile_name = findViewById(R.id.profile_name);
        profile_password = findViewById(R.id.profile_password);

        reference = FirebaseDatabase.getInstance().getReference().child("Users").child("customer").child(username_key_new);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                profile_email.setText(dataSnapshot.child("email").getValue().toString());
                profile_name.setText(dataSnapshot.child("nama_lengkap").getValue().toString());
                profile_password.setText(dataSnapshot.child("password").getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                simpan.setEnabled(false);
                simpan.setText("Loading..");
                reference = FirebaseDatabase.getInstance().getReference().child("Users").child("customer").child(username_key_new);
                reference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        dataSnapshot.getRef().child("email").setValue(profile_email.getText().toString());
                        dataSnapshot.getRef().child("nama_lengkap").setValue(profile_name.getText().toString());
                        dataSnapshot.getRef().child("password").setValue(profile_password.getText().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                Toast.makeText(ProfileAct.this, "Data berhasil diupdate!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ProfileAct.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    public void getUsernameLocal(){
        SharedPreferences sharedPreferences = getSharedPreferences(USERNAME_KEY, Context.MODE_PRIVATE);
        username_key_new = sharedPreferences.getString(username_key, "");
    }
}
