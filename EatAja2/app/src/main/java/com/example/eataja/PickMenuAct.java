package com.example.eataja;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class PickMenuAct extends AppCompatActivity {
LinearLayout best_seller1;
DatabaseReference reference;
TextView tv_restaurant, nama_menu, deskripsi, harga;
ImageView foto_menu, foto_toko;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_menu);

        foto_toko = findViewById(R.id.foto_toko);
        foto_menu = findViewById(R.id.foto_menu);
        tv_restaurant = findViewById(R.id.tv_restaurant);
        nama_menu = findViewById(R.id.nama_menu);
        deskripsi = findViewById(R.id.deskripsi);
        harga = findViewById(R.id.harga);

        Bundle bundle = getIntent().getExtras();
        final String toko = bundle.getString("toko");

        best_seller1 = findViewById(R.id.best_seller1);
        best_seller1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PickMenuAct.this, CartAct.class);
                intent.putExtra("cart", toko);
                startActivity(intent);
            }
        });

        reference = FirebaseDatabase.getInstance().getReference().child("menu").child(toko);
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                tv_restaurant.setText(dataSnapshot.child("nama_toko").getValue().toString());
                nama_menu.setText(dataSnapshot.child("nama").getValue().toString());
                deskripsi.setText(dataSnapshot.child("deskripsi").getValue().toString());
                harga.setText(dataSnapshot.child("harga").getValue().toString());

//                String url = dataSnapshot.child("foto").getValue().toString();
                Picasso.get().load(dataSnapshot.child("foto").getValue().toString()).resize(100, 100).centerCrop().into(foto_menu);
                Picasso.get().load(dataSnapshot.child("toko").getValue().toString()).resize(100,80).centerCrop().into(foto_toko);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
