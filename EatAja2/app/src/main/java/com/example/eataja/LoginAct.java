package com.example.eataja;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginAct extends AppCompatActivity {
    EditText editText_username;
    EditText editText_password;
    TextView textView;
    Button btn_login, btn_signup;
    DatabaseReference reference;


    String USERNAME_KEY = "usernamekey";
    String username_key = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editText_username = findViewById(R.id.editText_username);
        editText_password = findViewById(R.id.password);
        btn_login = findViewById(R.id.btn_login);
        btn_signup = findViewById(R.id.btn_signup);


        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent gotoregist = new Intent(LoginAct.this, Register.class);
                startActivity(gotoregist);
            }
        });


        btn_login.setEnabled(true);
        btn_login.setText("Login");
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_login.setEnabled(false);
                btn_login.setText("Loading..");
                final String username = editText_username.getText().toString();
                final String password = editText_password.getText().toString();

                reference = FirebaseDatabase.getInstance().getReference()
                        .child("Users").child("customer").child(username);

                reference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){

                            String firebasePassword = dataSnapshot.child("password").getValue().toString();

//                            Validasi
                            if(password.equals(firebasePassword)){


                                //                menyimpan data storage / hp kita
                                SharedPreferences sharedPreferences = getSharedPreferences(USERNAME_KEY, MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString(username_key, editText_username.getText().toString());
                                editor.apply();

                                Intent intent = new Intent(LoginAct.this, MainActivity.class);
                                startActivity(intent);
                            }else{
                                Toast.makeText(LoginAct.this, "Password salah", Toast.LENGTH_SHORT).show();
                            }



                        }else{
                            Toast.makeText(LoginAct.this, "Username tidak ditemukan", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(getApplicationContext(), "Database error", Toast.LENGTH_SHORT);
                    }
                });
            }
        });



    }
}
