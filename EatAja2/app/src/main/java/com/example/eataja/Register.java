package com.example.eataja;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Register extends AppCompatActivity {
    Button btn_register;
    EditText editText_fullname, editText_password, editText_phone, editText_name, editText_email;
    DatabaseReference reference;

    String USERNAME_KEY = "usernamekey";
    String username_key = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        editText_fullname = findViewById(R.id.editText_fullname);
        editText_email = findViewById(R.id.editText_email);
        editText_password = findViewById(R.id.editText_password);
        editText_phone = findViewById(R.id.editText_phone);
        editText_name = findViewById(R.id.editText_name);
        btn_register = findViewById(R.id.btn_register);


        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                menyimpan data storage / hp kita
                SharedPreferences sharedPreferences = getSharedPreferences(USERNAME_KEY, MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(username_key, editText_name.getText().toString());
                editor.apply();

//                simpan ke firebase
                reference = FirebaseDatabase.getInstance().getReference().child("Users").child("customer").child(editText_name.getText().toString());
                reference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        dataSnapshot.getRef().child("nama_lengkap").setValue(editText_fullname.getText().toString());
                        dataSnapshot.getRef().child("email").setValue(editText_email.getText().toString());
                        dataSnapshot.getRef().child("username").setValue(editText_name.getText().toString());
                        dataSnapshot.getRef().child("no_hp").setValue(editText_phone.getText().toString());
                        dataSnapshot.getRef().child("password").setValue(editText_password.getText().toString());
                        dataSnapshot.getRef().child("user_points").setValue(50000);
                        dataSnapshot.getRef().child("voucher").child("Discount 30%").child("0").setValue(true);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

//                pindah activity
                String name = editText_name.getText().toString();

                Toast.makeText(Register.this, name+ " berhasil terdaftar", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Register.this, LoginAct.class);
                startActivity(intent);
            }
        });



    }



}