package com.example.eataja;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

public class SplashAct extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);

        //seting untuk 1 detik
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                Intent gogetstarted = new Intent(SplashAct.this, LoginAct.class);
                startActivity(gogetstarted);
                finish();
            }
        }, 3000);
    }
}
