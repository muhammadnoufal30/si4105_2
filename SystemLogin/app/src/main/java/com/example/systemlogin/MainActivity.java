package com.example.systemlogin;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText username;
        Button btnLogin;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            username = (EditText) findViewById(R.id.username);
            btnLogin = (Button)findViewById(R.id.btnLogin);

            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String usernameKey = username.getText().toString();

                    if (usernameKey.equals("anisa@gmail.com")){
                        //jika login berhasil
                        Toast.makeText(getApplicationContext(), "LOGIN SUKSES",
                                Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(MainActivity.this, Welcome.class);
                        MainActivity.this.startActivity(intent);
                        finish();
                    }else {
                        //jika login gagal
                        Toast.makeText(getApplicationContext(), "LOGIN GAGAL SILAKAN DAFTAR",
                                Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(MainActivity.this, Registrasi.class);
                        MainActivity.this.startActivity(intent);
                        finish();
                        {

                        }
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setMessage("Email yang  Anda  Masukan Belum Terdaftar!")
                                .setNegativeButton("Retry", null).create().show();

                    }
                }

            });

        }
    }
