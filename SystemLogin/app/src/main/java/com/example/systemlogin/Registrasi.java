package com.example.systemlogin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Registrasi extends AppCompatActivity {

    private static final String LOG_TAG
            = Registrasi.class.getSimpleName();
    // Unique tag for the intent reply.
    public static final String EXTRA_MESSAGE =
            "com.example.android.SystemLogin.extra.MESSAGE";
    // Unique tag for the intent reply
    public static final int TEXT_REQUEST = 1;

    Button simpan;
    EditText mMessageEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);
        mMessageEditText = (EditText) findViewById(R.id.email);
        simpan = (Button) findViewById(R.id.simpan);

    }

    public void onClick(View view) {

        Log.d(LOG_TAG, "Button clicked!");
        Intent intent = new Intent(this, Welcome.class);
        String message = mMessageEditText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivityForResult(intent, TEXT_REQUEST);
    }
}







