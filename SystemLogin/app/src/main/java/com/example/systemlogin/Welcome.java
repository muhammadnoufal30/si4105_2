package com.example.systemlogin;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Welcome extends AppCompatActivity {
    EditText pass;
    Button btnExit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        pass = (EditText) findViewById(R.id.password);
        btnExit = (Button) findViewById(R.id.btnExite);

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String passKey = pass.getText().toString();

                if (passKey.equals("123456")){
                    //jika login berhasil
                    Toast.makeText(getApplicationContext(), "LOGIN SUKSES",
                            Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Welcome.this, MainActivity.class);
                    Welcome.this.startActivity(intent);
                    finish();
                }else {
                    //jika login gagal
                    Toast.makeText(getApplicationContext(), "PASSWORD YANG ANDA MASUKAN SALAH",
                            Toast.LENGTH_SHORT).show();
                    {

                    }

                }
            }

        });

    }
}

