package com.example.studykasus2;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    DatabaseHelper myDb;
    private TextView tvDetailnote, tvDetailharga, tvDetailnom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        setTitle("Detail Article");

        myDb = new DatabaseHelper(this);
        tvDetailnote = findViewById(R.id.denote);
        tvDetailharga = findViewById(R.id.tot);
        tvDetailnom = findViewById(R.id.nom);

        int id = getIntent().getIntExtra("Id", 0);
        Cursor cursor = myDb.getDataById(id);

        cursor.moveToNext();
        tvDetailnote.setText(cursor.getString(1));
        tvDetailharga.setText(cursor.getString(2));
        tvDetailnom.setText(cursor.getString(3));
    }
}
