package com.example.studykasus2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.database.Cursor;
import android.os.Bundle;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    RecyclerView rvListArticle;
    DatabaseHelper myDb;
    private ArrayList< Article> articles = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        setTitle("Orderan");
        rvListArticle = findViewById(R.id.list_nom);
        myDb = new DatabaseHelper(this);

        getDataArticle();

        rvListArticle.setLayoutManager(new LinearLayoutManager(this));
        rvListArticle.setAdapter(new ListAdapter(articles));
    }

    private void getDataArticle() {
        Cursor cursor = myDb.getAllData();

        while (cursor.moveToNext()){
            Article data = new Article();
            data.setId(cursor.getInt(0));
            data.setnote(cursor.getString(1));
            data.setharga(cursor.getString(2));
            data.setnom(cursor.getString(3));

            articles.add(data);
        }
    }
}
