package com.example.studykasus2;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

    public class CreatActivity extends AppCompatActivity {

        DatabaseHelper myDb;
        private EditText etTitle;
        TextView etAuthor, etArticle;
        private int mScore1;
        TextView mScore;
        static final String STATE_SCORE_1 = "Team 1 Score";

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_creat);

            setTitle("Create Article");

            myDb = new DatabaseHelper(this);
            etTitle = findViewById(R.id.Note);
            etAuthor = findViewById(R.id.Harga);
            etArticle = findViewById(R.id.nom);

            mScore = findViewById(R.id.nom);

            // Restores the scores if there is savedInstanceState.
            if (savedInstanceState != null) {
                mScore1 = savedInstanceState.getInt(STATE_SCORE_1);

                //Set the score text views
                mScore.setText(String.valueOf(mScore1));
            }
        }
        public void decreaseScore(View view) {
            // Get the ID of the button that was clicked.
            int viewID = view.getId();
            switch (viewID) {
                // If it was on Team 1:
                case R.id.imageView4:
                    // Decrement the score and update the TextView.
                    mScore1--;
                    mScore.setText(String.valueOf(mScore1));
                    break;
            }
        }

        public void increaseScore(View view) {
            // Get the ID of the button that was clicked.
            int viewID = view.getId();
            switch (viewID) {
                // If it was on Team 1:
                case R.id.imageView5:
                    // Increment the score and update the TextView.
                    mScore1++;
                    mScore.setText(String.valueOf(mScore1));
                    break;

            }
        }

        @Override
        protected void onSaveInstanceState(Bundle outState) {
            // Save the scores.
            outState.putInt(STATE_SCORE_1, mScore1);

            super.onSaveInstanceState(outState);
        }

        public void clickPost(View view) {
            String note = etTitle.getText().toString();
            String harga = etAuthor.getText().toString();
            String nom = etArticle.getText().toString();

            if (note.equals("") || harga.equals("") || nom.equals("")) {
                Toast.makeText(this, "Mohon isi data secara lengkap", Toast.LENGTH_SHORT).show();
            } else {
                boolean insert = myDb.insertData(note, harga, nom);

                    Toast.makeText(this, "Artikel berhasil di tambahkan", Toast.LENGTH_SHORT).show();

                }
            }
        }



