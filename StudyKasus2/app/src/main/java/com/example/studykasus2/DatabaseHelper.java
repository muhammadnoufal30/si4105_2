package com.example.studykasus2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    //nama database
    public static final String DATABASE_NAME = "SqlLite1.db";
    //nama table
    public static final String TABLE_NAME = "data_table";
    //versi database
    private static final int DATABASE_VERSION = 1;
    //table field
    public static final String COL_1 = "ID";
    public static final String COL_2 = "NOTE";
    public static final String COL_3 = "HARGA";
    public static final String COL_4 = "NOM";

    //constructor
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        SQLiteDatabase db1 = this.getWritableDatabase();
    }

    //create table pertamakali
    @Override
    public void onCreate(SQLiteDatabase db1) {
        db1.execSQL("create table data_table(id integer primary key autoincrement," +
                "note text, " +
                "harga text," +
                "nom text);");
    }

    //ketika database version diubah menjadi lebih tinggi
    @Override
    public void onUpgrade(SQLiteDatabase db1, int oldVersion, int newVersion) {
        db1.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db1);
    }

    //ketika database version diubah menjadi lebih rendah
    @Override
    public void onDowngrade(SQLiteDatabase db1, int oldVersion, int newVersion) {
        onUpgrade(db1, oldVersion, newVersion);
    }

    //metode untuk tambah data
    public boolean insertData(String note, String harga, String nom) {
        SQLiteDatabase db1 = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, note);
        contentValues.put(COL_3, harga);
        contentValues.put(COL_4, nom);
        long result = db1.insert(TABLE_NAME, null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    //metode untuk mengambil data
    public Cursor getAllData() {
        SQLiteDatabase db1 = this.getWritableDatabase();
        return db1.rawQuery("select * from data_table", null);
    }

    //metode untuk mengambil data
    public Cursor getDataById(int id) {
        SQLiteDatabase db1 = this.getWritableDatabase();
        return db1.rawQuery("select * from data_table where id = " + id, null);
    }

    //metode untuk merubah data
    public boolean updateData(String id, String note, String harga, String nom) {
        SQLiteDatabase db1 = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1, id);
        contentValues.put(COL_2, note);
        contentValues.put(COL_3, harga);
        contentValues.put(COL_4, nom);
        db1.update(TABLE_NAME, contentValues, "ID = ?", new String[]{id});
        return true;
    }

    //metode untuk menghapus data
    public int deleteData(String id) {
        SQLiteDatabase db1 = this.getWritableDatabase();
        return db1.delete(TABLE_NAME, "ID = ?", new String[]{id});
    }
}
