package com.example.studykasus2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("Home Article");

    }
    public void clickList(View view) {
        startActivity(new Intent(this, ListActivity.class));
    }

    public void clickCreate(View view) {
        startActivity(new Intent(this, CreatActivity.class));
    }

}